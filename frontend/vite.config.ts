import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'

export default defineConfig(() => {
	const config = {
		plugins: [react()],
		base: '/lottie-mini',
		resolve: {
			alias: {
				'@': path.resolve(__dirname, './src'),
			},
		},
    server: {
      port: 3333
    }
	}

	return config
})

import { useState } from 'react'
import { IconEye, IconTrash, IconDownload } from '@tabler/icons-react';
import { Player, Controls } from '@lottiefiles/react-lottie-player';

import TEST_ANIM_1 from './assets/test_anim_1.json'
import TEST_ANIM_2 from './assets/test_anim_2.json'
import TEST_ANIM_3 from './assets/test_anim_3.json'

import { Button } from './ui';
import cn from './common/utils/classnames';

const  App = () => {
  const [json, setJson] = useState(TEST_ANIM_3)
  const [modifiedJson, setModifiedJson] = useState(TEST_ANIM_3)
  const [showJson, setShowJson] = useState(false)
  const [selectedLayer, setSelectedLayer] = useState<undefined | string>(undefined)

  const getInvertedIndex = (i: number, length: number) => {
    // if selected index is 3, layers count is 5, so index in DOM is 5-1-3
    return (length -1) - i
  }

  const paintLayers = (lIndex: number, restoreDefaults: boolean) => {
    const lottieContainer = document.getElementById('lottie')
    const svgGroups = lottieContainer?.children[0]?.children[1].children;

    if (!svgGroups) return

    const groupIndex = getInvertedIndex(lIndex, json.layers.length);

    Array.from(svgGroups).forEach((item, index) => {
      if (restoreDefaults) {
        (item as HTMLElement).style.opacity = '1';
        (item as HTMLElement).style.outline = 'none';
        return
      }

      if (index === groupIndex) {
        (item as HTMLElement).style.outline = '1.5px solid #0d99ff';
        return
      }

      (item as HTMLElement).style.opacity = '0.4';

    })

  }

  const toggleLayer = (l: string, lIndex: number) => {
    paintLayers(lIndex, true)

    if (l === selectedLayer) {
      setSelectedLayer(undefined)
      return
    }

    setSelectedLayer(l)
    paintLayers(lIndex, false)
  }

  const renderLayers = () => {
    const layers = json.layers

  

    return layers.map((l, i) => (
      <div
        key={l.nm}
        className={cn('flex cursor-pointer flex-row gap-4 p-4 border-b-2 hover:bg-gray-200', {
          'bg-gray-200': selectedLayer === l.nm,
          'font-bold': selectedLayer === l.nm
        })}
        onClick={() => toggleLayer(l.nm, i)}
      >
        {l.nm}
        <Button className='ml-auto' title='delete layer'>
          <IconTrash  />
        </Button>
      </div>
    ))
  }

  const downloadJson = () => {
    const element = document.createElement('a');
    element.setAttribute('href',
        'data:text/plain;charset=utf-8, '
        + encodeURIComponent(JSON.stringify(modifiedJson)));
    element.setAttribute('download', `anim${Date.now()}.json`);
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  return (
    <div>
      {/* editing area */}
      <div className='flex'>
        <div className='w-1/5 h-screen p-4'>
          <div className='mb-2'>layers</div>
          {renderLayers()}
        </div>

        <div className='w-3/5 p-4 bg-gray-200'>
          <Player
            src={json}
            renderer="svg"
            autoplay
            loop
            className='bg-white !mt-20'
            style={{ height: '300px', width: '300px', marginBottom: '4rem' }}
          >
            <Controls visible={true} buttons={['play', 'repeat', 'frame', 'debug']} />
          </Player>
          <div className='flex flex-col m-4 justify-center gap-4'>
            <div className='flex gap-4'>
              <Button
                onClick={() => setShowJson((prev) => !prev)}
                className='flex gap-2  p-4 bg-[#00c1a2] text-white text-center '
              >
                <IconEye />
                {showJson ? 'hide json' : 'show json'}
              </Button>
            
              <Button
                onClick={downloadJson}
                className='flex gap-2  p-4 bg-[#00c1a2] text-white text-center '
              >
                <IconDownload />
                download json
              </Button>
            </div>
            {showJson ? (
              <textarea
                className='resize min-w-[10rem] min-h-[15rem] p-4'
                value={JSON.stringify(json, null, 2)}
              //  onChange={(e)=> setJson(e.target.value)}
              />
            ) :  null}
          </div>
        </div>
        <div className='w-1/5 p-4'>
          {selectedLayer ? (
            <div>
              selected layer: <br /> <b> {selectedLayer} </b>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  )
}

export default App

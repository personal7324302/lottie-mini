import { ComponentPropsWithoutRef } from 'react';

type ButtonProps = ComponentPropsWithoutRef<'button'>;

const Button = ({
  children,
  type = 'button',
  ...rest
}: ButtonProps) => (
  <button
    type={type || 'button'}
    {...rest}
  >
    {children}
  </button>
);

export default Button;
# mini lottie

## how to install

`nvm i`

`nvm use`

`cd frontend`

`npm run dev`

## what is done

## todos & bugs

- looks like CRDT or automerge should be used here
- looks like layers can be in different places of json, related bugs are possible

## resources and links

https://lottiefiles.github.io/lottie-docs/ - Lottie format documentation

https://lottiefiles.github.io/lottie-docs/playground/json_editor/ - Lottie JSON Viewer / Playground.

https://graphql.lottiefiles.com - LottieFiles GraphQL API
(Altair playground available at https://graphql.lottiefiles.com/graphiql)

https://github.com/LottieFiles/lottie-react

https://lottiefiles.github.io/lottie-docs/concepts/

https://lottiefiles.github.io/lottie-docs/breakdown/bouncy_ball/

https://lottiefiles.github.io/lottie-docs/layers/#text-layer

https://github.com/Mohitur669/Realtime-Collaborative-Code-Editor/blob/master/server.js
